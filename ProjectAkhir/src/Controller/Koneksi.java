package Controller;
import java.sql.*;
import java.util.logging.*;

public class Koneksi {
    private static Connection conn;
    public static Connection getConnection(){
        if (conn == null){
            try {
                Class.forName("com.mysql.jdbc.Driver");
                conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/projectPBO","root","");  
            }  catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(Koneksi.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    return conn;
    }
}
